// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBmiiciUAQgAb0nJq4dWjTlR7hI19MAhyw',
    authDomain: 'neoskiller-2decb.firebaseapp.com',
    databaseURL: 'https://neoskiller-2decb.firebaseio.com',
    projectId: 'neoskiller-2decb',
    storageBucket: 'neoskiller-2decb.appspot.com',
    messagingSenderId: '693385648736',
    appId: '1:693385648736:web:5f103d209a66728d3c8105',
    measurementId: 'G-3MBT1ER6HS',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
