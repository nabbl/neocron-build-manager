export class SubskillRules {
  bands = [
    {
      start: 0,
      end: 50,
      value: 1,
    },
    { start: 50, end: 75, value: 2 },
    { start: 75, end: 100, value: 3 },
  ];

  constructor() {}

  cost(subskillValue): number {
    return this.bands.reduce((cost, band) => {
      if (subskillValue >= band.start && subskillValue < band.end) {
        cost = band.value;
      }
      return cost;
    }, 5);
  }
  refund(subskillValue): number {
    return this.bands.reduce((refund, band) => {
      if (subskillValue > band.start && subskillValue <= band.end) {
        refund = band.value;
      }
      return refund;
    }, 5);
  }
}
