export enum Baseskills {
    int = 'Intelligence',
    str = 'Strength',
    con = 'Constitution',
    dex = 'Dexterity',
    psi = 'Psi Power'
}
