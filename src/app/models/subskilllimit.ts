export interface SubskillLimit {
    skill: string;
    cap: number;
    initial: number;
    initialSubskill: number;
}
