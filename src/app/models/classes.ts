import { SubskillLimit } from './subskilllimit';

export interface ClassLimit {
    class: string;
    limits: SubskillLimit[];
}
