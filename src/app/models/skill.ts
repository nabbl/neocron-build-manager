export interface Skill {
    [key: string]: number;
}
