
import { CharSetup } from './charsetup';

export interface NCCharacter {
    name: string;
    class: string;
    skills?: Record<string, number>;
    subSkillpoints?: Record<string, number>;
    calculatedSkill?: Record<string, number>;
    setup?: CharSetup;
    modifiers?: any;
    modifiersArmor?: any;
    protection?: Record<string, number>;
}

export enum NCClasses {
    pe = 'Private Eye',
    monk = 'Monk',
    tank = 'Tank',
    spy = 'Spy'
}
