import { DataEntry } from '../data/dataEntry';

export interface CharSetup {
  brain1?: DataEntry;
  brain2?: DataEntry;
  brain3?: DataEntry;
  brain4?: DataEntry;
  drugs1?: DataEntry;
  drugs2?: DataEntry;
  drugs3?: DataEntry;
  drugs4?: DataEntry;
  drugs5?: DataEntry;
  drugs6?: DataEntry;
  drugs7?: DataEntry;
  drugs8?: DataEntry;
  resistBuffs?: DataEntry;
  combatBuffs?: DataEntry;
  tradeskillBuffs?: DataEntry;
  heart?: DataEntry;
  eye?: DataEntry;
  backbone?: DataEntry;
  glove?: DataEntry;
  boneHead?: DataEntry;
  boneChest?: DataEntry;
  boneArm?: DataEntry;
  boneLeg?: DataEntry;
  boneFoot?: DataEntry;
  weapons?: DataEntry[];
}
