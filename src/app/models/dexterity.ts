export enum Dexterity {
    'pc', 'rc', 'tc', 'vhc', 'agl', 'rep', 'rec', 'rcl'
}
