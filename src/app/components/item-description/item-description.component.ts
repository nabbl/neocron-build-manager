import { Component, Input, OnInit } from '@angular/core';
import { SkillCalculationService } from "src/app/services/skill-calculation.service";
import { DataEntry } from 'src/app/data/dataEntry';

@Component({
  selector: 'app-item-description',
  templateUrl: './item-description.component.html',
  styleUrls: ['./item-description.component.scss']
})
export class ItemDescriptionComponent implements OnInit {

  @Input() item: DataEntry;
  @Input() hideReq: boolean;
  @Input() bigPicture: boolean;

  constructor(public skillCalculation: SkillCalculationService) { }

  ngOnInit(): void {
  }

}
