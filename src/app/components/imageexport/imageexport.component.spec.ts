import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageexportComponent } from './imageexport.component';

describe('ImageexportComponent', () => {
  let component: ImageexportComponent;
  let fixture: ComponentFixture<ImageexportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageexportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageexportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
