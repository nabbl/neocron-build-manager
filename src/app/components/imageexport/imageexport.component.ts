import {Component, OnInit} from '@angular/core';
import {NCCharacter} from 'src/app/models/character';
import {CharacterStoreService} from "src/app/services/character-store.service";
import { SkillCalculationService } from 'src/app/services/skill-calculation.service';

@Component({
  selector: 'app-imageexport',
  templateUrl: './imageexport.component.html',
  styleUrls: ['./imageexport.component.scss']
})
export class ImageexportComponent implements OnInit {
  character: NCCharacter;
  skills = ['int', 'str', 'con', 'dex', 'psi'];
  skillnames = {int: 'intelligence', str: 'strength', con: 'constitution', dex: 'dexterity', psi: 'psi power'};
  subskills = {
    int: ['hck', 'brt', 'psu', 'wep', 'cst', 'res', 'imp', 'wpw'],
    str: ['m-c', 'h-c', 'tra', 'for', 'pcr'],
    con: ['atl', 'hlt', 'end', 'fir', 'enr', 'xrr', 'por'],
    dex: ['p-c', 'r-c', 't-c', 'vhc', 'agl', 'rep', 'rec', 'rcl'],
    psi: ['apu', 'ppu', 'fcs', 'ppw', 'psr'],
  };
  implants = ['brain1', 'brain2', 'brain3', 'brain4', 'eye', 'heart', 'glove', 'backbone'];
  bones = ['boneChest', 'boneHead', 'boneArm', 'boneLeg', 'boneFoot'];
  armors = ['powerarmor', 'helmets', 'chokers', 'vests', 'belts', 'trousers', 'boots'];
  psis = ['combatBuffs', 'resistBuffs', 'tradeskillBuffs', 'shieldDeflector', 'shieldProtector', 'shieldAbsorber'];
  drugs = ['drugs1', 'drugs2', 'drugs3', 'drugs4', 'drugs5', 'drugs6', 'drugs7', 'drugs8'];

  constructor(
    private characterStore: CharacterStoreService,
    public skillCalculation: SkillCalculationService) {

    this.characterStore.character.subscribe((char) => {
      this.character = char;
    });
  }

  ngOnInit(): void {
  }

  hasItems(items) {
    let hasItems = false;
    items.forEach(item => {
      if (this.character.setup[item]) {
        hasItems = true;
      }
    });

    return hasItems;
  }
}
