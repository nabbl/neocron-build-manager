import {Component, Input, OnInit} from '@angular/core';
import { SkillCalculationService } from 'src/app/services/skill-calculation.service';
import {CharacterStoreService} from "../../../services/character-store.service";

@Component({
  selector: 'app-imageexport-item',
  templateUrl: './imageexport-item.component.html',
  styleUrls: ['./imageexport-item.component.scss']
})
export class ImageexportItemComponent implements OnInit {

  @Input() item: any;

  constructor(public skillCalculation: SkillCalculationService) {
  }

  ngOnInit(): void {
  }
}
