import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageexportItemComponent } from './imageexport-item.component';

describe('ImageexportItemComponent', () => {
  let component: ImageexportItemComponent;
  let fixture: ComponentFixture<ImageexportItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageexportItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageexportItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
