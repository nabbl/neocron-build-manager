import { Component, OnInit } from '@angular/core';
import { NCCharacter, NCClasses } from 'src/app/models/character';
import { ClassLimit } from 'src/app/models/classes';
import { SubskillRules } from 'src/app/models/subskillRules';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SubskillLimit } from 'src/app/models/subskilllimit';
import { CharacterStoreService } from 'src/app/services/character-store.service';
import { SkillCalculationService } from 'src/app/services/skill-calculation.service';
import { defer, of, Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss'],
})
export class CalculatorComponent {
  characterUpdate: Subject<void> = new Subject();
  character: NCCharacter;

  classLimits: ClassLimit[] = [
    {
      class: 'pe',
      limits: [
        { skill: 'int', cap: 60, initial: 3, initialSubskill: 20 },
        { skill: 'str', cap: 60, initial: 3, initialSubskill: 40 },
        { skill: 'con', cap: 65, initial: 3, initialSubskill: 10 },
        { skill: 'dex', cap: 80, initial: 3, initialSubskill: 78 },
        { skill: 'psi', cap: 35, initial: 1, initialSubskill: 2 },
      ],
    },
    {
      class: 'spy',
      limits: [
        { skill: 'int', cap: 100, initial: 5, initialSubskill: 78 },
        { skill: 'str', cap: 40, initial: 2, initialSubskill: 10 },
        { skill: 'con', cap: 40, initial: 2, initialSubskill: 20 },
        { skill: 'dex', cap: 100, initial: 3, initialSubskill: 40 },
        { skill: 'psi', cap: 20, initial: 1, initialSubskill: 2 },
      ],
    },
    {
      class: 'tank',
      limits: [
        { skill: 'int', cap: 25, initial: 1, initialSubskill: 12 },
        { skill: 'str', cap: 100, initial: 5, initialSubskill: 78 },
        { skill: 'con', cap: 100, initial: 4, initialSubskill: 40 },
        { skill: 'dex', cap: 75, initial: 2, initialSubskill: 20 },
        { skill: 'psi', cap: 0, initial: 0, initialSubskill: 0 },
      ],
    },
    {
      class: 'monk',
      limits: [
        { skill: 'int', cap: 100, initial: 4, initialSubskill: 40 },
        { skill: 'str', cap: 20, initial: 1, initialSubskill: 2 },
        { skill: 'con', cap: 45, initial: 1, initialSubskill: 10 },
        { skill: 'dex', cap: 35, initial: 2, initialSubskill: 20 },
        { skill: 'psi', cap: 100, initial: 5, initialSubskill: 78 },
      ],
    },
  ];

  subskills = {
    int: ['hck', 'brt', 'psu', 'wep', 'cst', 'res', 'imp', 'wpw'],
    str: ['m-c', 'h-c', 'tra', 'for', 'pcr'],
    con: ['atl', 'hlt', 'end', 'fir', 'enr', 'xrr', 'por'],
    dex: ['p-c', 'r-c', 't-c', 'vhc', 'agl', 'rep', 'rec', 'rcl'],
    psi: ['apu', 'ppu', 'fcs', 'ppw', 'psr'],
  };

  private subskillRules = new SubskillRules();

  constructor(
    private snackBar: MatSnackBar,
    private characterStore: CharacterStoreService,
    public skillCalculation: SkillCalculationService
  ) {
    this.characterStore.character.subscribe((char) => {
      this.character = char;
      this.filterLimits({ value: char.class });
    });

    this.skillCalculation.initCharacterSkills();

    this.characterUpdate.pipe(debounceTime(300)).subscribe(() => {
      this.characterStore.character.next(this.character);
    });
  }

  capCharacter() {
    this.skillCalculation.selectedClassLimits.limits.forEach(limit => {
      this.incrementTo(limit.skill, true, limit.skill);
    });
    this.characterUpdate.next();
  }

  filterLimits(event) {
    this.skillCalculation.selectedClassLimits = this.classLimits.find(
      (record) =>
        record.class ===
        Object.keys(NCClasses).find((key) => NCClasses[key] === event.value)
    );
    this.skillCalculation.selectedClassLimits.limits.forEach((limit: SubskillLimit) => {
      if (!this.character.skills[limit.skill]) {
        this.character.skills[limit.skill] = limit.initial;
      }
    });
  }

  increment(skill: string, isBaseskill: boolean, mainSkill: string) {
    if (!this.character.skills[mainSkill]) {
      this.character.skills[mainSkill] = 0;
    }
    if (!this.character.skills[skill]) {
      this.character.skills[skill] = 0;
    }
    if (!this.character.subSkillpoints[skill]) {
      this.character.subSkillpoints[skill] = 0;
    }
    if (isBaseskill) {
      const capValue = this.skillCalculation.selectedClassLimits.limits.find(
        (limit: SubskillLimit) => mainSkill === limit.skill
      ).cap;
      if (this.character.skills[skill] >= capValue) {
        this.character.skills[skill] = capValue;
      } else {
        this.character.skills[skill]++;
        this.character.subSkillpoints[skill] += 5;
      }
    } else {
      const subskillCost = this.subskillRules.cost(
        this.character.skills[skill]
      );
      const parentSkill = Object.keys(this.subskills).find((key) =>
        this.subskills[key].includes(skill)
      );
      if (this.character.subSkillpoints[parentSkill] >= subskillCost) {
        this.character.subSkillpoints[parentSkill] -= subskillCost;
        this.character.skills[skill]++;
      }
    }
    this.characterUpdate.next();
  }

  incrementTo(skill: string, isBaseskill: boolean, mainSkill: string, maxValue?: number) {
    if (!this.character.skills[skill]) {
      this.character.skills[skill] = 0;
    }
    let capValue;
    if (isBaseskill) {
      capValue = this.skillCalculation.selectedClassLimits.limits.find(
        (limit: SubskillLimit) => mainSkill === limit.skill
      ).cap;
    } else {
      capValue = maxValue !== undefined ? maxValue : 255;
    }
    let loopBreak = 0;
    if (this.character.skills[skill] < capValue) {
      while (this.character.skills[skill] < capValue && loopBreak <= 255) {
        this.increment(skill, isBaseskill, mainSkill);
        loopBreak++;
      }
    } else {
      while (this.character.skills[skill] > capValue && loopBreak <= 255) {
        this.decrement(skill, isBaseskill, mainSkill);
        loopBreak++;
      }
    }

    this.characterUpdate.next();
  }

  decrement(skill: string, isBaseskill: boolean, mainSkill: string) {
    if (
      this.character.skills[skill] <= 0 ||
      isNaN(this.character.skills[skill])
    ) {
      this.character.skills[skill] = 0;
      return;
    }
    if (isBaseskill) {
      const initialValue = this.skillCalculation.selectedClassLimits.limits.find(
        (limit: SubskillLimit) => mainSkill === limit.skill
      ).initial;
      if (this.character.skills[skill] === initialValue) {
        return;
      }
      this.subskills[skill].forEach((element: string) => {
        this.character.skills[element] = 0;
      });
      this.character.skills[skill]--;
      this.skillCalculation.refundSubskill(mainSkill);
    } else {
      const refund = this.subskillRules.refund(this.character.skills[skill]);
      this.character.subSkillpoints[mainSkill] += refund;
      this.character.skills[skill]--;
    }
    this.characterUpdate.next();
  }

  updateCharacterBaseskill(skill: string) {
    const capValue = this.skillCalculation.selectedClassLimits.limits.find(
      (limit: SubskillLimit) => skill === limit.skill
    ).cap;
    if (this.character.skills[skill] >= Number(capValue)) {
      this.character.skills[skill] = Number(capValue);
    }

    this.subskills[skill].forEach((element: string) => {
      this.character.skills[element] = 0;
    });

    this.skillCalculation.refundSubskill(skill);
  }

  updateCharacterSubskill(subSkill: string, mainSkill: string, maxValue: string) {
    this.incrementTo(subSkill, false, mainSkill, Number(maxValue));
  }
}
