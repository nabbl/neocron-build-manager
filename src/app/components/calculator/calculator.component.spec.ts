import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalculatorComponent } from './calculator.component';
import { NCCharacter } from 'src/app/models/character';
import { MatSnackBarModule, MatSnackBar } from '@angular/material/snack-bar';
import { MatBottomSheet, MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatCardModule } from '@angular/material/card';

fdescribe('CalculatorComponent', () => {
  let component: CalculatorComponent;
  let fixture: ComponentFixture<CalculatorComponent>;
  const character: NCCharacter = {
    name: 'New Character',
    class: 'Private Eye',
    skills: {},
    subSkillpoints: {},
    setup: {},
    modifiers: {},
    modifiersArmor: {}
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ MatSnackBarModule, MatBottomSheetModule, MatListModule,
        MatButtonModule, MatInputModule, MatSelectModule,
        BrowserAnimationsModule, MatIconModule, MatExpansionModule,
        MatCardModule],
      declarations: [ CalculatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.character = {
      name: 'New Character',
      class: 'Private Eye',
      skills: {},
      subSkillpoints: {},
      setup: {},
      modifiers: {},
      modifiersArmor: {}
    };
    component.filterLimits({ value: 'Monk'});
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('increments Baseskill', () => {
    component.character.skills.int = 10;
    component.increment('int', true);
    expect(component.character.skills.int).toEqual(11);
    expect(component.character.subSkillpoints.int).toEqual(5);
  });

  it('increments Subskill', () => {
    component.character.subSkillpoints.int = 50;
    component.character.skills.hck = 20;
    component.increment('hck', false);
    expect(component.character.skills.hck).toEqual(21);
    expect(component.character.subSkillpoints.int).toEqual(49);
  });

  it('decrements Baseskill', () => {
    component.character.subSkillpoints.int = 50;
    component.character.skills.hck = 20;
    component.character.skills.int = 20;
    component.decrement('int', true);
    expect(component.character.skills.int).toEqual(19);
    expect(component.character.skills.hck).toEqual(0);
    expect(component.character.subSkillpoints.int).toEqual(120);
  });

  it('decrements Subskill', () => {
    component.character.subSkillpoints.int = 50;
    component.character.skills.hck = 20;
    component.decrement('hck', false);
    expect(component.character.skills.hck).toEqual(19);
    expect(component.character.subSkillpoints.int).toEqual(51);
  });

  it('decrements Subskill on inputValue change', () => {
    component.character.subSkillpoints.int = 50;
    component.character.skills.hck = 20;
    component.updateCharacterSubskill('hck', '0');
    expect(component.character.skills.hck).toEqual(0);
    expect(component.character.subSkillpoints.int).toEqual(70);
  });
});
