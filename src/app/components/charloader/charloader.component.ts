import { Component, OnInit, Inject } from '@angular/core';
import {
  MatBottomSheetRef,
  MAT_BOTTOM_SHEET_DATA,
} from '@angular/material/bottom-sheet';
import { CharacterStoreService } from 'src/app/services/character-store.service';

@Component({
  selector: 'app-charloader',
  templateUrl: './charloader.component.html',
  styleUrls: ['./charloader.component.scss'],
})
export class CharloaderComponent implements OnInit {
  constructor(
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
    private bottomSheetRef: MatBottomSheetRef<CharloaderComponent>,
    private characterService: CharacterStoreService
  ) {
  }

  ngOnInit(): void {}

  openLink(name: string): void {
    this.bottomSheetRef.dismiss(name);
  }

  delete(name: string): void {
    this.data = this.data.filter(e => e !== name);
    this.characterService.removeItem(name);
  }
}
