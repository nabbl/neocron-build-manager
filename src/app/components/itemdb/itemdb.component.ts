import {Component, OnInit} from '@angular/core';
import backbones from 'src/app/data/backbones.json';
import brain from 'src/app/data/brainimplants.json';
import bodyunderwear from 'src/app/data/bodyunderwear.json';
import eyes from 'src/app/data/eyeimplants.json';
import gloves from 'src/app/data/gloves.json';
import psigloves from 'src/app/data/psicombatgloves.json';
import hearts from 'src/app/data/hearts.json';
import bones from 'src/app/data/bones.json';
import powerarmors from 'src/app/data/powerarmor.json';
import holovests from 'src/app/data/holo-vests.json';
import helmets from 'src/app/data/helmets.json';
import chokers from 'src/app/data/neckarmour.json';
import vests from 'src/app/data/vests.json';
import belts from 'src/app/data/belts.json';
import trousers from 'src/app/data/pants.json';
import boots from 'src/app/data/boots.json';
import drugsData from 'src/app/data/drugs.json';
import drones from 'src/app/data/drones.json';
import hacknet from 'src/app/data/hacknetsoftware.json';
import ppumodules from 'src/app/data/ppupsimodules.json';
import heavyweapons from 'src/app/data/heavyweapons.json';
import meleeweapons from 'src/app/data/meleeweapons.json';
import rifles from 'src/app/data/rifles.json';
import pistols from 'src/app/data/pistols.json';
import apumodules from 'src/app/data/apupsimodules.json';
import {DataEntry} from 'src/app/data/dataEntry';
import {CharacterStoreService} from 'src/app/services/character-store.service';
import {NCCharacter} from 'src/app/models/character';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ItemdatabaseService} from 'src/app/services/itemdatabase.service';
import {SkillCalculationService} from 'src/app/services/skill-calculation.service';

@Component({
  selector: 'app-itemdb',
  templateUrl: './itemdb.component.html',
  styleUrls: ['./itemdb.component.scss'],
})
export class ItemdbComponent implements OnInit {
  character: NCCharacter;

  items = {};

  filterTerm = '';

  step = '';

  currentTab: string;

  baseSkills = ['int', 'dex', 'psi', 'con', 'str'];

  baseSkillSorting = {
    tank: ['str', 'con', 'dex', 'int', 'psi', 'others'],
    pe: ['dex', 'str', 'con', 'int', 'psi', 'others'],
    spy: ['dex', 'int', 'str', 'con', 'psi', 'others'],
    monk: ['psi', 'int', 'con', 'dex', 'str', 'others'],
  };

  implants: {
    abrain: any;
    beyes: any;
    chearts: any;
    dgloves: any;
    ebackbones: any;
    fbones: any;
  };

  armor: {
    apowerarmor: any;
    bhelmets: any;
    cchokers: any;
    dvests: any;
    ebelts: any;
    ftrousers: any;
    gboots: any;
  };

  drugs: {
    adrugs: any;
  };

  psi: {
    aresistbuffs: any;
    bcombatbuffs: any;
    ctradeskillbuffs: any;
  };

  weapons: {
    aheavy: any;
    bmelee?: any;
    crifle?: any;
    dpistol?: any;
    eapu?: any;
    fppu?: any;
    gdrones?: any;
    hhacknet?: any;
  };

  selectedIndex = 0;
  completeItemList: {};

  constructor(
    private characterStore: CharacterStoreService,
    private snackBar: MatSnackBar,
    private itemDatabase: ItemdatabaseService,
    public skillCalculation: SkillCalculationService
  ) {
    this.characterStore.character.subscribe((character) => {
      this.character = character;
      this.updateItemData(character);
    });

    this.itemDatabase.tabIndex.subscribe((index) => {
      this.selectedIndex = index;
    });

    this.itemDatabase.selectedCharacterTab.subscribe((tabName) => {
      this.chooseTab(tabName);
    });

    this.itemDatabase.applyFilterTerm.subscribe((empty) => {
      this.applyFilterTerm();
    });
  }

  setStep(skillName) {
    if (this.isAlwaysExpanded(skillName)) {
      return;
    }
    this.step = skillName;
  }

  isAlwaysExpanded(skillName) {
    switch (skillName) {
      case 'drugs':
      case 'resist':
      case 'combat':
      case 'trade':
        return true;
    }
    return false;
  }

  chooseTab(tabName): void {
    this.currentTab = tabName;
    switch (tabName.toLowerCase()) {
      case 'implants':
        this.items = this.filterEmptyTabs(this.implants);
        break;
      case 'drugs':
        this.items = this.filterEmptyTabs(this.drugs);
        break;
      case 'psi':
        this.items = this.filterEmptyTabs(this.psi);
        break;
      case 'armor':
        this.items = this.filterEmptyTabs(this.armor);
        break;
      case 'weapons':
        this.items = this.filterEmptyTabs(this.weapons);
        break;
      case 'protection':
        this.items = {};
        break;
    }
  }

  ngOnInit(): void {
  }

  filterEmptyTabs(items: object) {
    const newItems = {};
    Object.keys(items).forEach((key) => {
      if (Object.keys(items[key]).length > 0 || items[key].length > 0) {
        newItems[key] = items[key];
      }
    });
    return newItems;
  }

  updateItemData(char: NCCharacter, searchTerm?: string) {
    this.implants = {
      abrain: this.filterForRequirement(brain, char, searchTerm),
      beyes: this.filterForRequirement(eyes, char, searchTerm),
      chearts: this.filterForRequirement(hearts, char, searchTerm),
      dgloves: this.filterForRequirement(
        gloves.concat(psigloves),
        char,
        searchTerm
      ),
      ebackbones: this.filterForRequirement(backbones, char, searchTerm),
      fbones: this.filterForRequirement(bones, char, searchTerm),
    };
    const paHoloBody = powerarmors.concat(holovests).concat(bodyunderwear);
    this.armor = {
      apowerarmor: this.filterForRequirement(paHoloBody, char, searchTerm),
      bhelmets: this.filterForRequirement(helmets, char, searchTerm),
      cchokers: this.filterForRequirement(chokers, char, searchTerm),
      dvests: this.filterForRequirement(vests, char, searchTerm),
      ebelts: this.filterForRequirement(belts, char, searchTerm),
      ftrousers: this.filterForRequirement(trousers, char, searchTerm),
      gboots: this.filterForRequirement(boots, char, searchTerm),
    };

    this.drugs = {
      adrugs: [
        ['drugs', this.filterSimpleItems(drugsData, searchTerm)]
      ]
    };

    this.psi = {
      aresistbuffs: [
        [
          'resist',
          this.filterSimpleItems(ppumodules.filter((psimodule) =>
            psimodule.name.toLowerCase().includes('resist booster')
          ), searchTerm),
        ],
      ],
      bcombatbuffs: [
        [
          'combat',
          this.filterSimpleItems(ppumodules.filter((psimodule) =>
            psimodule.name.toLowerCase().includes('combat booster')
          ), searchTerm),
        ],
      ],
      ctradeskillbuffs: [
        [
          'trade',
          this.filterSimpleItems(ppumodules.filter(
            (psimodule) =>
              psimodule.name.toLowerCase().includes('booster') &&
              !psimodule.name.toLowerCase().includes('combat booster') &&
              !psimodule.name.toLowerCase().includes('resist booster')
          ), searchTerm),
        ],
      ],
    };

    this.weapons = {
      aheavy: this.filterForRequirement(heavyweapons, char, searchTerm),
      bmelee: this.filterForRequirement(meleeweapons, char, searchTerm),
      crifle: this.filterForRequirement(rifles, char, searchTerm),
      dpistol: this.filterForRequirement(pistols, char, searchTerm),
      eapu: this.filterForRequirement(apumodules, char, searchTerm),
      fppu: this.filterForRequirement(ppumodules, char, searchTerm),
      gdrones: this.filterForRequirement(drones, char, searchTerm),
      hhacknet: this.filterForRequirement(hacknet, char, searchTerm),
    };

    if (this.currentTab) {
      this.chooseTab(this.currentTab);
    }
  }

  filterSimpleItems(array: DataEntry[], searchTerm) {
    const newArray = [];
    array.forEach((dataEntry) => {
      if (this.charHasImplant(dataEntry)) {
        return;
      }

      if (typeof searchTerm !== 'undefined' && !dataEntry.name.toLowerCase().includes(searchTerm.toLowerCase())) {
        return;
      }

      newArray.push(dataEntry);
    });

    return newArray;
  }

  filterForRequirement(
    array: DataEntry[],
    char: NCCharacter,
    searchTerm?: string
  ) {
    const newArray = [];
    array = array.sort((a: any, b: any) => (a.tl < b.tl ? 1 : -1));
    array.forEach((dataEntry) => {
      if (this.charHasImplant(dataEntry)) {
        return;
      }

      let requirementMet = true;
      if (typeof searchTerm !== 'undefined' && !dataEntry.name.toLowerCase().includes(searchTerm.toLowerCase())) {
        return false;
      }

      dataEntry.requirements.forEach((requirement) => {
        if (requirement.shortName.indexOf('woc') >= 0) {
          return;
        }

        let modifier = 0;
        if (dataEntry.modifiers) {
          modifier =
            dataEntry.modifiers.find(
              (value) => value.shortName === requirement.shortName
            )?.value || 0;
        }

        let requiredValue = requirement.value;
        if (this.isBaseSkill(requirement.shortName.toLowerCase())) {
          requiredValue = requirement.value - modifier;
        }

        if (
          (char.skills[requirement.shortName.toLowerCase()] || 0) +
          (char.modifiers[requirement.shortName.toLowerCase()] || 0) < requiredValue
        ) {
          requirementMet = false;
        }

        if (
          requirement.shortName.toLowerCase() === 'class' && requirement.value &&
          requirement.value.toLowerCase() !== this.character.class.toLowerCase()
        ) {
          requirementMet = false;
        }
      });

      if (requirementMet) {
        let requirement = 'others';
        dataEntry.requirements.forEach((element) => {
          if (this.isBaseSkill(element.shortName.toLowerCase())) {
            requirement = element.shortName.toLowerCase();
          }
        });
        if (!newArray[requirement]) {
          newArray[requirement] = [];
        }
        newArray[requirement].push(dataEntry);
      }
    });
    const charClass = this.characterStore.getCharacterClass(char);
    // needs sorting based on character class
    const sortedArray = Object.entries(newArray).sort((a: any, b: any) => {
      return (
        this.baseSkillSorting[charClass].indexOf(a[0]) -
        this.baseSkillSorting[charClass].indexOf(b[0])
      );
    });

    return sortedArray;
  }

  // this is ugly but we need to do it for backwards compatability
  cleanType(implant: DataEntry): DataEntry {
    switch (implant.type) {
      case 'brain implants':
        implant.type = 'brain';
        break;
      case 'backbones':
        implant.type = 'backbone';
        break;
      case 'hearts':
        implant.type = 'heart';
        break;
      case 'eye implants':
        implant.type = 'eye';
        break;
      case 'psi combat gloves':
      case 'gloves':
        implant.type = 'glove';
        break;
      case 'bones':
        const bonesName = implant.name.toLowerCase().split(' ');
        if (bonesName.includes('arm')) {
          implant.type = 'boneArm';
        }
        if (bonesName.includes('leg')) {
          implant.type = 'boneLeg';
        }
        if (bonesName.includes('chest')) {
          implant.type = 'boneChest';
        }
        if (bonesName.includes('head')) {
          implant.type = 'boneHead';
        }
        if (bonesName.includes('foot')) {
          implant.type = 'boneFoot';
        }
        break;
      case 'pants':
        implant.type = 'trousers';
        break;
      case 'neck armour':
        implant.type = 'chokers';
        break;
      case 'power armor':
      case 'body underwear':
      case 'holo-vests':
        implant.type = 'powerarmor';
        break;
      case 'ppu psi modules':
        const psiModuleName = implant.name.toLowerCase().split(' ');
        if (psiModuleName.includes('booster')) {
          if (psiModuleName.includes('combat')) {
            implant.type = 'combatBuffs';
          }
          if (psiModuleName.includes('resist')) {
            implant.type = 'resistBuffs';
          }
          if (psiModuleName.includes('supporter') || psiModuleName.includes('spy') || psiModuleName.includes('construction')) {
            implant.type = 'tradeskillBuffs';
          }
        }
        break;
    }

    if (
      implant.type.includes('weapons') ||
      implant.type.includes('rifles') ||
      implant.type.includes('pistols') ||
      implant.type.includes('hacknet') ||
      implant.type.includes('drones') ||
      implant.type.includes('module')
    ) {
      implant.type = 'weapons';
    }
    return implant;
  }

  addToSetup(implant: DataEntry) {
    implant = this.cleanType(implant);
    let implantType = implant.type;
    let hasBeenAdded = false;
    if (implantType === 'brain' || implantType === 'drugs') {
      let max = 9;
      if (implantType === 'brain') {
        max = 5;
      }
      for (let i = 1; i <= max; i++) {
        if (i === 5 && !hasBeenAdded) {
          this.snackBar.open('No Free Slot for item found', '', {
            duration: 5000,
          });
          break;
        }
        // we need to check if character has this brain implant.. you cant put it twice
        if (
          this.character.setup[implantType + i] &&
          this.character.setup[implantType + i].name === implant.name
        ) {
          this.snackBar.open('Character already has object', '', {
            duration: 5000,
          });
          break;
        } else {
          if (!this.character.setup[implantType + i]) {
            implantType = implantType + i;
            hasBeenAdded = true;
            break;
          }
        }
      }
      if (hasBeenAdded) {
        this.character.setup[implantType] = implant;
      }
    } else if (implantType === 'weapons') {
      if (!this.character.setup[implantType]) {
        this.character.setup[implantType] = [];
      }
      this.character.setup[implantType].push(implant);
    } else {
      this.character.setup[implantType] = implant;
    }

    this.characterStore.updateModifier(this.character);
    this.itemDatabase.applyFilterTerm.next('');
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.filterTerm = filterValue;
    this.itemDatabase.applyFilterTerm.next('');
  }

  applyFilterTerm() {
    this.updateItemData(this.character, this.filterTerm);
  }

  charHasImplant(implant: DataEntry): boolean {
    let hasImplant = Object.values(this.character.setup).find(
      (value) => value.name === implant.name
    );

    for (const [key, value] of Object.entries(this.character.setup)) {
      if (key === 'weapons') {
        value.forEach((weapon) => {
          if (weapon.name === implant.name) {
            hasImplant = true;
            return;
          }
        });
      }
    }

    return hasImplant;
  }

  isBaseSkill(skill: string) {
    return this.baseSkills.includes(skill);
  }
}
