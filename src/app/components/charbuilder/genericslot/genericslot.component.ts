import { Component, OnInit, Input } from '@angular/core';
import { NCCharacter } from 'src/app/models/character';
import { CharacterStoreService } from 'src/app/services/character-store.service';
import { ItemdatabaseService } from 'src/app/services/itemdatabase.service';

@Component({
  selector: 'app-genericslot',
  templateUrl: './genericslot.component.html',
  styleUrls: ['./genericslot.component.scss'],
})
export class GenericslotComponent implements OnInit {
  @Input() character: NCCharacter;

  @Input() itemName: string;

  label: string;

  constructor(
    private characterStore: CharacterStoreService,
    private itemDatabase: ItemdatabaseService,
  ) {}

  ngOnInit(): void {
    this.label = this.itemName.replace('bone', '').replace(/([A-Z])/g, ' $1');
  }

  removeSlot(impSlot: string) {
    if (!this.character.setup[impSlot]) {
      let category = impSlot;
      if (impSlot.includes('brain')) {
        category = 'brain';
      }
      if (impSlot.includes('bone') && !impSlot.includes('back')) {
        category = 'bone';
      }
      const indizes = this.itemDatabase.getIndizes();
      const index = indizes[category];
      this.itemDatabase.tabIndex.next(index);
    }
    delete this.character.setup[impSlot];
    this.characterStore.updateModifier(this.character);
    this.itemDatabase.applyFilterTerm.next('');
  }
}
