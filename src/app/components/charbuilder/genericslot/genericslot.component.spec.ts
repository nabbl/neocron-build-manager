import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericslotComponent } from './genericslot.component';

describe('GenericslotComponent', () => {
  let component: GenericslotComponent;
  let fixture: ComponentFixture<GenericslotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenericslotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericslotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
