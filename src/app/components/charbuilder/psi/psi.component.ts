import { Component, OnInit, Input } from '@angular/core';
import { NCCharacter } from 'src/app/models/character';

@Component({
  selector: 'app-psi',
  templateUrl: './psi.component.html',
  styleUrls: ['./psi.component.scss']
})
export class PsiComponent implements OnInit {

  @Input() character: NCCharacter;

  constructor() { }

  ngOnInit(): void {
  }

}
