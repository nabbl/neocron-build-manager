import { Component, OnInit } from '@angular/core';
import { CharacterStoreService } from 'src/app/services/character-store.service';
import { NCCharacter, NCClasses } from 'src/app/models/character';
import { ItemdatabaseService } from 'src/app/services/itemdatabase.service';

@Component({
  selector: 'app-charbuilder',
  templateUrl: './charbuilder.component.html',
  styleUrls: ['./charbuilder.component.scss'],
})
export class CharbuilderComponent implements OnInit {
  character: NCCharacter;
  charclass: string;
  drugFlashIndex = 0;

  constructor(
    private characterStore: CharacterStoreService,
    private itemDatabase: ItemdatabaseService
  ) {
    this.characterStore.character.subscribe((char) => {
      this.character = char;
      this.charclass = Object.keys(NCClasses).find(
        (key) => NCClasses[key] === char.class
      );
      this.drugFlashIndex = 0;
      Object.keys(this.character.setup).map((key) => {
        if (key.includes('drugs')) {
          this.drugFlashIndex += 1;
        } else {
          this.drugFlashIndex = 0;
        }
      });
    });
  }

  ngOnInit(): void {}

  switchToTab(event) {
    this.itemDatabase.selectedCharacterTab.next(event.tab.textLabel);
  }
}
