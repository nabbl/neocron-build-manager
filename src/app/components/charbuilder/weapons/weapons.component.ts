import { Component, OnInit, Input } from '@angular/core';
import { NCCharacter } from 'src/app/models/character';
import { CharacterStoreService } from 'src/app/services/character-store.service';
import { DataEntry } from 'src/app/data/dataEntry';



@Component({
  selector: 'app-weapons',
  templateUrl: './weapons.component.html',
  styleUrls: ['./weapons.component.scss']
})
export class WeaponsComponent implements OnInit {

  @Input() character: NCCharacter;
  charClass: string;

  constructor(private characterStoreService: CharacterStoreService) { }

  ngOnInit(): void {
    this.charClass = this.characterStoreService.getCharacterClass(
      this.character
    );
  }

  removeSlot(weapon: DataEntry) {
    const index = this.character.setup.weapons.indexOf(weapon);
    this.character.setup.weapons.splice(index, 1);
    this.characterStoreService.updateModifier(this.character);
  }
}
