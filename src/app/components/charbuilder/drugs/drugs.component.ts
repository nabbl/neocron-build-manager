import { Component, OnInit, Input } from '@angular/core';
import { NCCharacter } from 'src/app/models/character';

@Component({
  selector: 'app-drugs',
  templateUrl: './drugs.component.html',
  styleUrls: ['./drugs.component.scss']
})
export class DrugsComponent implements OnInit {

  @Input() character: NCCharacter;
  @Input() drugFlashIndex = 0;

  constructor() { }

  ngOnInit(): void {
  }

}
