import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharbuilderComponent } from './charbuilder.component';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatCardModule } from '@angular/material/card';

describe('CharbuilderComponent', () => {
  let component: CharbuilderComponent;
  let fixture: ComponentFixture<CharbuilderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ MatSnackBarModule, MatBottomSheetModule, MatListModule,
        MatButtonModule, MatInputModule, MatSelectModule,
        BrowserAnimationsModule, MatIconModule, MatExpansionModule,
        MatCardModule, MatTabsModule],
      declarations: [ CharbuilderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharbuilderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
