import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { NCCharacter } from 'src/app/models/character';
import { CharacterStoreService } from 'src/app/services/character-store.service';

@Component({
  selector: 'app-armor',
  templateUrl: './armor.component.html',
  styleUrls: ['./armor.component.scss']
})
export class ArmorComponent implements OnInit, OnChanges {

  @Input() character: NCCharacter;
  charClass: string;

  constructor(private characterStoreService: CharacterStoreService) { }

  ngOnInit(): void {
    this.charClass = this.characterStoreService.getCharacterClass(this.character);
  }

  ngOnChanges(changes: any) {
    this.charClass = this.characterStoreService.getCharacterClass(
      this.character
    );
  }

  calculateResist(slot: string): any {
    return this.characterStoreService.calculateResist(slot, this.character);
  }

}
