import {Component, Input, OnInit} from '@angular/core';
import {NCCharacter, NCClasses} from 'src/app/models/character';
import {CharacterStoreService} from 'src/app/services/character-store.service';
import {Subject} from 'rxjs';
import {debounceTime} from 'rxjs/operators';

@Component({
  selector: 'app-protection',
  templateUrl: './protection.component.html',
  styleUrls: ['./protection.component.scss']
})
export class ProtectionComponent implements OnInit {
  @Input() character: NCCharacter;
  characterUpdate: Subject<void> = new Subject();

  allProtection = 0;

  resists = ['for', 'pcr', 'fir', 'enr', 'xrr', 'por'];

  defaultProtections = { foreignCastHoly: 38, selfCastHoly: 56, selfCastBlessed: 32, injectNanites: 35 };

  constructor(private characterStore: CharacterStoreService) {
    this.characterStore.character.subscribe((character) => {
      this.character = character;
      if (typeof this.character.protection === 'undefined') {
        this.character.protection = {};
      }
    });

    this.characterUpdate.pipe(debounceTime(300)).subscribe(() => {
      this.characterStore.character.next(this.character);
    });
  }

  ngOnInit(): void {
  }

  setAllProtectionValue(value: number) {
    this.allProtection = value;
    this.resists.forEach((resist) => {
      this.character.protection[resist] = value;
    });

    this.characterUpdate.next();
  }

  updateProtectionValue(resist: string, value: number) {
    this.character.protection[resist] = value;
    this.characterUpdate.next();
  }

  increment(resistName?: string) {
    if (typeof resistName !== 'undefined') {
      this.character.protection[resistName]++;
    } else {
      this.allProtection++;
      this.resists.forEach((resist) => {
        this.character.protection[resist] = this.allProtection;
      });
    }
    this.characterUpdate.next();
  }

  decrement(resistName?: string) {
    if (typeof resistName !== 'undefined') {
      if (this.character.protection[resistName] <= 0) {
        return;
      }
      this.character.protection[resistName]--;
    } else {
      if (this.allProtection > 0) {
        this.allProtection--;
      }
      this.resists.forEach((resist) => {
        this.character.protection[resist] = this.allProtection;
      });
    }
    this.characterUpdate.next();
  }
}
