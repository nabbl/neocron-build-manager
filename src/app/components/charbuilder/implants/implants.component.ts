import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { NCCharacter } from 'src/app/models/character';
import { CharacterStoreService } from 'src/app/services/character-store.service';

@Component({
  selector: 'app-implants',
  templateUrl: './implants.component.html',
  styleUrls: ['./implants.component.scss'],
})
export class ImplantsComponent implements OnInit, OnChanges {
  @Input() character: NCCharacter;
  charClass: string;

  constructor(private characterStoreService: CharacterStoreService) {}

  ngOnInit(): void {
    this.charClass = this.characterStoreService.getCharacterClass(
      this.character
    );
  }

  ngOnChanges(changes: any) {
    this.charClass = this.characterStoreService.getCharacterClass(
      this.character
    );
  }
}
