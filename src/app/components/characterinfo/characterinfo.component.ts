import { Component, OnInit } from '@angular/core';
import {CharloaderComponent} from "src/app/components/charloader/charloader.component";
import {SavedChar} from "src/app/models/savedchar";
import {SharingComponent} from "src/app/components/sharing/sharing.component";
import {MatSnackBar} from "@angular/material/snack-bar";
import {CharacterStoreService} from "src/app/services/character-store.service";
import {MatBottomSheet} from "@angular/material/bottom-sheet";
import {AngularFirestore} from "@angular/fire/firestore";
import {NCCharacter, NCClasses} from "src/app/models/character";
import { SkillCalculationService } from 'src/app/services/skill-calculation.service';
import * as firebase from 'firebase';
import * as htmlToImage from 'html-to-image';
import * as downloadJs from 'downloadjs';

@Component({
  selector: 'app-characterinfo',
  templateUrl: './characterinfo.component.html',
  styleUrls: ['./characterinfo.component.scss']
})
export class CharacterinfoComponent implements OnInit {

  character: NCCharacter;
  classes = Object.values(NCClasses);

  constructor(
    private snackBar: MatSnackBar,
    private characterStore: CharacterStoreService,
    public skillCalculation: SkillCalculationService,
    private bottomSheet: MatBottomSheet,
    private firestore: AngularFirestore
  ) {
    this.characterStore.character.subscribe((char) => {
      this.character = char;
    });
  }

  ngOnInit(): void {
  }

  saveCharacter() {
    this.characterStore.saveCharacter();
    this.snackBar.open('Character saved', '', {
      duration: 5000,
    });
  }

  createCharacter() {
    this.characterStore.create();
    this.skillCalculation.initCharacterSkills();
    this.snackBar.open('New Character created', '', {
      duration: 5000,
    });
  }

  loadCharacter() {
    const charList = this.characterStore.getCharacters();
    //   this.characterStore.getCharacters()
    // ).map((key) => key.replace('savedData_', ''));
    const bottomSheetRef = this.bottomSheet.open(CharloaderComponent, {
      data: charList,
    });
    bottomSheetRef.afterDismissed().subscribe((name: string) => {
      this.characterStore.loadCharacter(name);
      this.snackBar.open('Character loaded', '', {
        duration: 5000,
      });
    });
  }

  shareCharacter() {
    const str = JSON.stringify(this.character);
    const bytes = new TextEncoder().encode(str);
    const blob = firebase.firestore.Blob.fromUint8Array(bytes);
    this.firestore.collection<SavedChar>('sharedCharacters').add({ blob }).then((docRef) => this.bottomSheet.open(SharingComponent, {
      data:  window.location.origin + '?character=' + docRef.id,
    })).catch(err => console.log(err));
  }

  exportCharacter() {
    const node = document.getElementById('image-export');
    const wrapper = document.getElementById('image-export-wrapper');
    if (node === null && wrapper === null) {
      return;
    }

    wrapper.classList.add('show');
    this.openLoadingIndicator();
    htmlToImage.toPng(node, { backgroundColor: '#202020', width: 1150, pixelRatio: 1 })
      .then(dataUrl => {
        wrapper.classList.remove('show');
        this.closeLoadingIndicator();
        downloadJs(dataUrl, this.getFileName());
      })
      .catch(error => console.error('oops, something went wrong!', error));
  }

  openLoadingIndicator() {
    const overlay = document.getElementById('loading-overlay');
    if (overlay === null) {
      return;
    }
    overlay.classList.add('show');
  }

  closeLoadingIndicator() {
    const overlay = document.getElementById('loading-overlay');
    if (overlay === null) {
      return;
    }
    overlay.classList.remove('show');
  }

  getFileName() {
    let fileName = this.character.name;
    fileName = fileName.trim();
    fileName = fileName.toLowerCase();
    fileName = fileName.replace(/[^a-zA-Z0-9-_ ]/gi, '');
    fileName = fileName.replace(/ /g, '_');

    return fileName + '.png';
  }

  changeClass(charClass) {
    this.characterStore.create(charClass.value);
    this.skillCalculation.initCharacterSkills();
  }

}
