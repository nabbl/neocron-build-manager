import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResistviewerComponent } from './resistviewer.component';

describe('ResistviewerComponent', () => {
  let component: ResistviewerComponent;
  let fixture: ComponentFixture<ResistviewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResistviewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResistviewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
