import {Component, OnInit} from '@angular/core';
import * as Highcharts from 'highcharts';
import {CharacterStoreService} from 'src/app/services/character-store.service';
import {NCCharacter, NCClasses} from 'src/app/models/character';
import DarkGreenTheme from 'highcharts/themes/high-contrast-dark.src.js';
import {simplify, parse} from 'mathjs';
import {ArmorResistService} from 'src/app/services/resists/armor-resist.service';
import {SkillResistService} from 'src/app/services/resists/skill-resist.service';

DarkGreenTheme(Highcharts);

@Component({
  selector: 'app-resistviewer',
  templateUrl: './resistviewer.component.html',
  styleUrls: ['./resistviewer.component.scss'],
})
export class ResistviewerComponent {
  character: NCCharacter;

  constructor(private characterStore: CharacterStoreService,
              public armorResistService: ArmorResistService,
              public skillResistService: SkillResistService) {
    this.characterStore.character.subscribe((character) => {
      this.ncClass = Object.keys(NCClasses).find(
        (key) => NCClasses[key] === character.class
      );
      this.character = character;
      this.updateCharts(character);
    });
  }

  resists: any = [];
  hlt: any;
  runspeed = 0;
  formulas = {
    hlt: {
      con: {
        tippingPoint: '75',
        equation1: '(x/75) * 0.21',
        equation2: '(((x-75)/49)^0.66666 + 0.7) * 0.25',
      },
      hlt: {
        tippingPoint: '75',
        equation1: '(x/75) * 0.35',
        equation2: '(((x-75)/19)^0.66666 + 0.7) * 0.35',
      },
    },
  };

  categories = ['for', 'pcr', 'fir', 'enr', 'xrr', 'por'];

  Highcharts: typeof Highcharts = Highcharts; // required
  ncClass = 'pe';
  isPpuThreshold = 110;

  resiCaps = {
    pe: 84,
    tank: 88,
    apu: 76,
    ppu: 92,
    spy: 80,
  };

  chartOptions: Highcharts.Options = {
    series: [
      {
        data: this.resists,
        type: 'bar',
      },
    ],
  };

  // optional function, defaults to null
  ngOnInit(): void {
  }

  updateCharts(character: NCCharacter) {
    this.resists = this.calculateResists(character);
    this.hlt = this.calculateHlt(character);

    this.chartOptions = {
      colors: [
        'rgba(255, 255, 255, 0.4)',
        'rgba(85, 85, 85, 0.4)',
        'rgba(255, 0, 0, 0.4)',
        'rgba(30, 30, 255, 0.4)',
        'rgba(148, 0, 210, 0.4)',
        'rgba(0, 128, 0, 0.4)'
      ],
      chart: {
        style: {
          fontFamily: 'Visitor TT1',
          fontSize: '12px',
          fontWeight: 'normal',
          backgroundColor: 'transparent'
        },
        animation: false,
        type: 'bar',
        height: 450,
      },
      credits: {
        enabled: false,
      },
      tooltip: {
        shared: true,
        animation: false,
        pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: {point.y}%<br/>',
        backgroundColor: '#2d2d2d',
        borderColor: '#2d2d2d',
        borderRadius: 0,
        style: {
          fontSize: '10px',
          fontWeight: 'normal'
        },
      },
      plotOptions: {
        bar: {
          pointPadding: 0.2,
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            format: '{point.y}%',
            style: {
              fontSize: '10px',
              fontWeight: 'normal'
            },
          },
        },
      },
      series: this.resists,
      title: {
        text: '',
      },
      xAxis: {
        categories: ['Head', 'Body', 'Legs'],
        crosshair: {
          color: '#191919'
        },
        labels: {
          style: {
            fontSize: '10px',
            fontWeight: 'normal'
          },
        },
      },
      legend: {
        verticalAlign: 'top',
        itemStyle: {
          fontWeight: 'normal'
        }
      },
      yAxis: {
        labels: {
          overflow: 'justify'
        },
        gridLineColor: '#191919',
        max: 100,
        min: 0,
        title: {
          text: 'Resists',
        },
        plotLines: [
          {
            color: '#82DF50',
            width: 1,
            value: this.getResistCap(),
            dashStyle: 'LongDashDotDot',
          },
        ],
      },
    };
  }

  getResistCap() {
    if (this.ncClass === 'monk') {
      if (typeof this.character.skills.ppu !== 'undefined' &&
        (this.character.skills.ppu + this.character.modifiers.ppu) >= this.isPpuThreshold) {
        return this.resiCaps.ppu;
      }

      return this.resiCaps.apu;
    }

    return this.resiCaps[this.ncClass];
  }

  calculateHlt(character: NCCharacter): any {
    if (!character.skills.con || !character.skills.hlt) {
      return 0;
    }

    let i = 1;
    let k = 1;

    // TODO: There is actually a different formular at work after 75 HLT and CON. Need to find it
    if (character.skills.con >= 75) {
      i = 1;
    }
    if (character.skills.hlt >= 75) {
      k = 1;
    }

    const conFormula = this.formulas.hlt.con['equation' + i];
    const hltFormula = this.formulas.hlt.hlt['equation' + k];

    const conValue =
      (character.skills.con || 0) + (character.modifiers.con || 0);
    const hltValue =
      (character.skills.hlt || 0) + (character.modifiers.hlt || 0);

    const equationcon = parse(conFormula);
    const s1 = simplify(equationcon);
    const con = s1.evaluate({x: conValue});

    const equationhlt = parse(hltFormula);
    const s2 = simplify(equationhlt);
    const hlt = s2.evaluate({x: hltValue});
    return Math.round((con + hlt + 0.2) * 1200);
  }

  calculateResists(character: NCCharacter) {
    const charClass = Object.keys(NCClasses).find(
      (key) => NCClasses[key] === character.class
    );

    const resists = {
      head: [0, 0, 0, 0, 0, 0],
      body: [0, 0, 0, 0, 0, 0],
      legs: [0, 0, 0, 0, 0, 0]
    };

    const protectionResists = [0, 0, 0, 0, 0, 0];
    const armorResists = this.armorResistService.calculateArmorResists(this.character, this.categories, charClass);
    const skillResists = this.skillResistService.calculateSkillResists(this.character, this.categories);

    let index = 0;
    this.categories.forEach((resist) => {
      if (typeof this.character.protection !== 'undefined' &&
        typeof this.character.protection[resist] !== 'undefined') {
        protectionResists[index] = this.character.protection[resist];
      }
      index++;
    });

    for (const [region, resistValues] of Object.entries(resists)) {
      index = 0;
      resistValues.forEach((value => {
        resists[region][index] = this.summedResists(skillResists[index],
          armorResists[region][index],
          protectionResists[index]);
        index++;
      }));
    }

    const series = [];

    index = 0;
    this.categories.forEach((skill) => {
      series[index] = {
        name: skill,
        data: [
          resists.head[index],
          resists.body[index],
          resists.legs[index],
        ]
      };
      index++;
    });

    return series;
  }

  summedResists(skillResist, armorResist, protectionResist) {
    let damage = 100;
    damage = damage - (damage * protectionResist / 100);
    damage = damage - (damage * skillResist / 100);
    damage = damage - (damage * armorResist / 100);

    const calculatedResists = 100 - damage;
    const summedResists = Math.round(calculatedResists * 100) / 100;

    const resistCap = this.getResistCap();
    if (summedResists > resistCap) {
      return resistCap;
    }

    return summedResists;
  }
}
