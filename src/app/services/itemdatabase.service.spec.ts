import { TestBed } from '@angular/core/testing';

import { ItemdatabaseService } from './itemdatabase.service';

describe('ItemdatabaseService', () => {
  let service: ItemdatabaseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ItemdatabaseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
