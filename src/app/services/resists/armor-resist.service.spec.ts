import { TestBed } from '@angular/core/testing';

import { ArmorResistService } from './armor-resist.service';

describe('ArmorResistService', () => {
  let service: ArmorResistService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ArmorResistService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
