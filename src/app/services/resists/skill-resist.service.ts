import {Injectable} from '@angular/core';
import {simplify, parse} from 'mathjs';

@Injectable({
  providedIn: 'root'
})
export class SkillResistService {
  resistCap = 70;
  tippingPoint = 75;

  pointValue = 0.4853;
  pointValueCorrections = {
    enr: 1.44,
    fir: 1.44,
    por: 1.44,
    xrr: 1.44,
    for: 1.009,
    pcr: 1.009
  };

  formulaBeforeTP = 'y * x';

  conSkills = ['enr', 'fir', 'por', 'xrr'];
  strSkills = ['pcr', 'for'];

  skillResistCon = {
    76: 54.650,
    77: 55.960,
    78: 57.059,
    79: 58.040,
    80: 58.941,
    81: 59.783,
    82: 60.580,
    83: 61.339,
    84: 62.067,
    85: 62.769,
    86: 63.447,
    87: 64.105,
    88: 64.745,
    89: 65.369,
    90: 65.978,
    91: 66.574,
    92: 67.157,
    93: 67.729,
    94: 68.291,
    95: 68.842,
    96: 69.385,
    97: 69.919
  };

  skillResistStr = {
    76: 38.293,
    77: 39.211,
    78: 39.981,
    79: 40.668,
    80: 41.300,
    81: 41.890,
    82: 42.448,
    83: 42.980,
    84: 43.490,
    85: 43.982,
    86: 44.457,
    87: 44.918,
    88: 45.367,
    89: 45.804,
    90: 46.231,
    91: 46.648,
    92: 47.057,
    93: 47.458,
    94: 47.851,
    95: 48.238,
    96: 48.618,
    97: 48.992,
    98: 49.36,
    99: 49.724,
    100: 50.082,
    101: 50.435,
    102: 50.784,
    103: 51.129,
    104: 51.469,
    105: 51.806,
    106: 52.139,
    107: 52.468,
    108: 52.794,
    109: 53.117,
    110: 53.436,
    111: 53.752,
    112: 54.066,
    113: 54.377,
    114: 54.685,
    115: 54.990,
    116: 55.293,
    117: 55.593,
    118: 55.891,
    119: 56.187,
    120: 56.480,
    121: 56.772,
    122: 57.061,
    123: 57.348,
    124: 57.633,
    125: 57.916,
    126: 58.197,
    127: 58.477,
    128: 58.755,
    129: 59.030,
    130: 59.305,
    131: 59.577,
    132: 59.848,
    133: 60.118,
    134: 60.385,
    135: 60.652,
    136: 60.916,
    137: 61.180,
    138: 61.442,
    139: 61.702,
    140: 61.962,
    141: 62.219,
    142: 62.476,
    143: 62.731,
    144: 62.985,
    145: 63.238,
    146: 63.490,
    147: 63.740,
    148: 63.990,
    149: 64.238,
    150: 64.485,
    151: 64.731,
    152: 64.976,
    153: 65.219,
    154: 65.462,
    155: 65.704,
    156: 65.945,
    157: 66.184,
    158: 66.423,
    159: 66.661,
    160: 66.898,
    161: 67.134,
    162: 67.369,
    163: 67.603,
    164: 67.836,
    165: 68.068,
    166: 68.300,
    167: 68.531,
    168: 68.760,
    169: 68.989,
    170: 69.218,
    171: 69.445,
    172: 69.672,
    173: 69.897
  };

  resists = [0, 0, 0, 0, 0, 0];

  constructor() {
  }

  calculateSkillResists(character, categories) {
    categories.forEach(skill => {
      const index = categories.indexOf(skill);
      const value = (character.skills[skill] || 0) + (character.modifiers[skill] || 0);
      if (value > this.tippingPoint) {
        this.resists[index] = this.calculateAfterTippingPoint(value, skill);
      } else {
        this.resists[index] = this.calculateBeforeTippingPoint(value, skill);
      }
    });

    return this.resists;
  }

  calculateBeforeTippingPoint(value, skill) {
    const skillEquation = simplify(parse(this.formulaBeforeTP));

    return skillEquation.evaluate({
      y: value,
      x: (this.pointValue * this.pointValueCorrections[skill]),
    });
  }

  calculateAfterTippingPoint(value, skill) {
    let resist = this.resistCap;
    if (this.conSkills.includes(skill)) {
      resist = this.skillResistCon[value];
    } else if (this.strSkills.includes(skill)) {
      resist = this.skillResistStr[value];
    }

    if (typeof resist === 'undefined') {
      return this.resistCap;
    }

    return resist;
  }
}
