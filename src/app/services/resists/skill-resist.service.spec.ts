import { TestBed } from '@angular/core/testing';

import { SkillResistService } from './skill-resist.service';

describe('SkillResistService', () => {
  let service: SkillResistService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SkillResistService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
