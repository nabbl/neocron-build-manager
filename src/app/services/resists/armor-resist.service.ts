import {Injectable} from '@angular/core';
import {simplify, parse} from 'mathjs';
import {CharacterStoreService} from 'src/app/services/character-store.service';

@Injectable({
  providedIn: 'root'
})
export class ArmorResistService {

  armorCorrections = {
    pe: 0.45,
    tank: 0.45,
    spy: 0.45,
    monk: 0.45,
  };

  formular = 'sqrt((x-x%2)/y) * z * 100';

  resists = {
    head: [0, 0, 0, 0, 0, 0],
    body: [0, 0, 0, 0, 0, 0],
    legs: [0, 0, 0, 0, 0, 0]
  };

  constructor(public characterStore: CharacterStoreService) {
  }

  calculateArmorResists(character, categories, charClass) {
    const armorValues = [
      {type: 'head', value: this.characterStore.calculateResist('head', character)},
      {type: 'body', value: this.characterStore.calculateResist('body', character)},
      {type: 'legs', value: this.characterStore.calculateResist('legs', character)}
    ];

    const armorEquation = simplify(parse(this.formular));
    armorValues.forEach((armorRegion) => {
      categories.forEach(skill => {
        const index = categories.indexOf(skill);
        if (typeof armorRegion.value[skill] !== 'undefined') {
          this.resists[armorRegion.type][index] = armorEquation.evaluate({
            x: armorRegion.value[skill],
            y: (255 * this.armorCorrections[charClass]),
            z: this.armorCorrections[charClass]
          });
        } else {
          this.resists[armorRegion.type][index] = 0;
        }
      });
    });

    return this.resists;
  }
}
