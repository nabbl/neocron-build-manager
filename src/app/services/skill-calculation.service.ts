import { Injectable } from '@angular/core';
import {SubskillLimit} from "src/app/models/subskilllimit";
import {ClassLimit} from "src/app/models/classes";
import {CharacterStoreService} from "src/app/services/character-store.service";
import {debounceTime} from "rxjs/operators";
import {NCCharacter} from "src/app/models/character";
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SkillCalculationService {
  characterUpdate: Subject<void> = new Subject();

  public selectedClassLimits: ClassLimit = undefined;
  character: NCCharacter;

  constructor(
    private characterStore: CharacterStoreService) {

    this.characterStore.character.subscribe((char) => {
      this.character = char;
    });
    this.characterUpdate.pipe(debounceTime(300)).subscribe(() => {
      this.characterStore.character.next(this.character);
    });
  }

  initCharacterSkills() {
    this.selectedClassLimits.limits.forEach(limit => {
      this.refundSubskill(limit.skill);
    });
  }

  refundSubskill(mainSkill) {
    const initialSubskills = this.selectedClassLimits.limits.find(
      (limit: SubskillLimit) => mainSkill === limit.skill
    ).initialSubskill;
    const initialValue = this.selectedClassLimits.limits.find(
      (limit: SubskillLimit) => mainSkill === limit.skill
    ).initial;

    this.character.subSkillpoints[mainSkill] =
      initialSubskills +
      +((this.character.skills[mainSkill] - initialValue) * 5);
    this.characterUpdate.next();
  }

  calculateSkill(modifier: any, skill: any): number {
    skill = Number(skill);
    modifier = Number(modifier);
    if (isNaN(skill)) {
      if (!isNaN(modifier) && modifier > 0) {
        return modifier;
      }
      return 0;
    }
    if (isNaN(modifier)) {
      return skill;
    }
    if ((modifier + skill) < 0) {
      return 0;
    }
    return modifier + skill;
  }

  formatModifier(modifier: any): string {
    modifier = Number(modifier);

    return (modifier > 0 ? '+' + modifier : modifier);
  }
}
