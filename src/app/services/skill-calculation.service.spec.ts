import { TestBed } from '@angular/core/testing';

import { SkillCalculationService } from './skill-calculation.service';

describe('SkillCalculationService', () => {
  let service: SkillCalculationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SkillCalculationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
