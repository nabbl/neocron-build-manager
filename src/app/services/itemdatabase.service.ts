import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ItemdatabaseService {

  tabIndex: BehaviorSubject<number> = new BehaviorSubject(0);
  selectedCharacterTab: BehaviorSubject<string> = new BehaviorSubject('implants');
  applyFilterTerm: BehaviorSubject<string> = new BehaviorSubject('');

  indizes = {
    brain: 0,
    eye: 1,
    heart: 2,
    glove: 3,
    backbone: 4,
    bone: 5,
    powerarmor: 0,
    helmets: 1,
    chokers: 2,
    vests: 3,
    belts: 4,
    trousers: 5,
    boots: 6

  };

  constructor() { }

  public getIndizes() {
    return this.indizes;
  }

}
