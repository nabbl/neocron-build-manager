import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { NCCharacter, NCClasses } from '../models/character';
import * as _ from 'lodash';
import { ActivatedRoute } from '@angular/router';
import { filter } from 'rxjs/operators';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/firestore';
import { SavedChar } from '../models/savedchar';

@Injectable({
  providedIn: 'root',
})
export class CharacterStoreService {
  characterTemplate: NCCharacter = {
    name: 'New Character',
    class: NCClasses.pe,
    skills: {},
    subSkillpoints: {},
    setup: {},
    modifiers: {},
    modifiersArmor: {},
    protection: {},
  };

  localStorageItems: {};

  character: BehaviorSubject<NCCharacter> = new BehaviorSubject(
    _.cloneDeep(this.characterTemplate)
  );

  char: NCCharacter;

  constructor(
    private route: ActivatedRoute,
    private firestore: AngularFirestore
  ) {
    this.route.queryParams
      .pipe(filter((params) => params.character))
      .subscribe((params) => {
        const document: AngularFirestoreDocument = this.firestore
          .collection<SavedChar>('sharedCharacters')
          .doc(params.character);
        document.get().subscribe((result) => {
          const sharedChar = result.data();
          if (!sharedChar) {
            return;
          }
          const blob = sharedChar.blob.toUint8Array();
          const str = new TextDecoder().decode(blob);
          const character = JSON.parse(str);
          this.character.next(character);
        });
      });
  }

  public saveCharacter() {
    this.character.subscribe((character) => {
      localStorage.setItem(
        'savedData_' + character.name.trim(),
        JSON.stringify(character)
      );
    });
  }

  public create(charClass?: string) {
    const newChar = _.cloneDeep(this.characterTemplate);
    if (charClass) {
      newChar.class = charClass;
    }
    this.updateModifier(newChar);
  }

  public getCharacters() {
    return Object.keys({ ...localStorage })
      .filter((x) => x.includes('savedData_'))
      .map((key) => key.replace('savedData_', ''));
  }

  loadCharacter(name: string) {
    const items = { ...localStorage };
    this.updateModifier(JSON.parse(items['savedData_' + name.trim()]));
  }

  getCharacterClass(character) {
    return Object.keys(NCClasses).find(
      (key) => NCClasses[key] === character.class
    );
  }

  public updateModifier(char: NCCharacter) {
    char.modifiersArmor = {};
    char.modifiers = {};
    if (Object.entries(char.setup).length === 0) {
      char.modifiers = {};
      this.character.next(char);
      return;
    }

    Object.values(char.setup).forEach((value) => {
      if (!value.modifiers) { return; }
      value.modifiers.map((entry) => {
        if (entry.type && entry.type === 'armor') {
          if (char.modifiersArmor[entry.shortName.toLowerCase()]) {
            char.modifiersArmor[entry.shortName.toLowerCase()] += (entry.value || 0);
          } else {
            char.modifiersArmor[entry.shortName.toLowerCase()] = entry.value;
          }
        } else {
          if (char.modifiers[entry.shortName.toLowerCase()]) {
            char.modifiers[entry.shortName.toLowerCase()] += entry.value;
          } else {
            char.modifiers[entry.shortName.toLowerCase()] = entry.value;
          }
        }
      });
    });
    this.character.next(char);
  }

  public removeItem(name: string) {
    localStorage.removeItem('savedData_' + name);
  }

  public calculateResist(slot: string, character: NCCharacter): any {
    const resists = {};
    let regions = [];
    switch (slot) {
      case 'head':
        regions = ['helmets', 'chokers', 'powerarmor'];
        break;
      case 'body':
        regions = ['vests', 'belts', 'powerarmor'];
        break;
      case 'legs':
        regions = ['trousers', 'boots', 'powerarmor'];
        break;
    }
    regions.forEach((region) => {
      if (!character.setup[region]) {
        return;
      }
      character.setup[region].modifiers.map((modifier) => {
        if (modifier.type === 'armor') {
          if (!resists[modifier.shortName.toLowerCase()]) {
            resists[modifier.shortName.toLowerCase()] = modifier.value;
          } else {
            resists[modifier.shortName.toLowerCase()] += modifier.value;
          }
        }
      });
    });
    return resists;
  }
}
