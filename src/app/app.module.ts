import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { MatTabsModule } from '@angular/material/tabs';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { HighchartsChartModule } from 'highcharts-angular';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { CalculatorComponent } from './components/calculator/calculator.component';
import { ItemdbComponent } from './components/itemdb/itemdb.component';
import { CharbuilderComponent } from './components/charbuilder/charbuilder.component';
import { ResistviewerComponent } from './components/resistviewer/resistviewer.component';
import { CharloaderComponent } from './components/charloader/charloader.component';
import { DebounceClickDirective } from './directives/debounce-click.directive';
import { SharingComponent } from './components/sharing/sharing.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAnalyticsModule } from '@angular/fire/analytics';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from 'src/environments/environment';
import { GenericslotComponent } from './components/charbuilder/genericslot/genericslot.component';
import { ImplantsComponent } from './components/charbuilder/implants/implants.component';
import { ArmorComponent } from './components/charbuilder/armor/armor.component';
import { DrugsComponent } from './components/charbuilder/drugs/drugs.component';
import { PsiComponent } from './components/charbuilder/psi/psi.component';
import { ProtectionComponent } from './components/charbuilder/protection/protection.component';
import { WeaponsComponent } from './components/charbuilder/weapons/weapons.component';
import { HttpClientModule } from '@angular/common/http';
import { TooltipRendererDirective } from './directives/tooltiprenderer.directive';
import { TooltipComponent } from './components/tooltip/tooltip.component';
import { ImageexportComponent } from './components/imageexport/imageexport.component';
import { ImageexportItemComponent } from './components/imageexport/imageexport-item/imageexport-item.component';
import { CharacterinfoComponent } from './components/characterinfo/characterinfo.component';
import { ItemDescriptionComponent } from './components/item-description/item-description.component';

@NgModule({
  declarations: [
    AppComponent,
    CalculatorComponent,
    ItemdbComponent,
    CharbuilderComponent,
    ResistviewerComponent,
    CharloaderComponent,
    DebounceClickDirective,
    SharingComponent,
    GenericslotComponent,
    ImplantsComponent,
    ArmorComponent,
    DrugsComponent,
    PsiComponent,
    WeaponsComponent,
    TooltipRendererDirective,
    TooltipComponent,
    ImageexportComponent,
    ImageexportItemComponent,
    CharacterinfoComponent,
    ItemDescriptionComponent,
    ProtectionComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatCardModule,
    MatSelectModule,
    MatIconModule,
    MatListModule,
    MatButtonModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatSnackBarModule,
    MatTabsModule,
    FormsModule,
    MatBottomSheetModule,
    HighchartsChartModule,
    ClipboardModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAnalyticsModule,
    AngularFirestoreModule,
    HttpClientModule
  ],
  entryComponents: [
    CharloaderComponent,
    TooltipComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
