export interface DataEntry {
  name: string;
  type: string;
  image?: ImageModel;
  tl?: number;
  modifiers: ModifierModel[];
  requirements?: RequirementModel[];
}

export interface ImageModel {
  name?: string;
  x?: number;
  y?: number;
  url?: string;
}

export interface RequirementModel {
  name?: string;
  shortName: string;
  value: any;
}

export interface ModifierModel {
  name?: string;
  shortName: string;
  value: number;
  type?: string;
}
